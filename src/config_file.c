/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <modal_json.h>
#include "config_file.h"


#define FILE_HEADER "\
/**\n\
 * voxl-mavlink-server Configuration File\n\
 * UART fields are for APQ8096 VOXL1 or QRB5165 VOXL2 with external fc\n\
 * UDP fields are for PX4 on SDSP on QRB5165 only\n\
 * External FC field is for QRB5165 only. Set to true to enable UART\n\
 * communication to an external flight controller, otherwise a UDP interface\n\
 * will be started to talk to voxl-px4 on localhost which is the default behavior.\n\
 *\n\
 */\n"

int px4_uart_bus;
int px4_uart_baudrate;
int udp_port_to_px4;
int udp_port_from_px4;
int external_fc;


#ifdef APQ8096
#define DEFAULT_UART_BUS 5
#define DEFAULT_UART_BACKUP_BUS 12 // Port J11 on VOXL1
int px4_uart_backup_bus;
#else
#define DEFAULT_UART_BUS 1
#endif

int config_file_print(void)
{
	printf("=================================================================");
	printf("\n");
	printf("Parameters as loaded from config file:\n");
	printf("px4_uart_bus:               %d\n", px4_uart_bus);
	#ifdef APQ8096
	printf("px4_uart_backup_bus:               %d\n", px4_uart_backup_bus);
	#endif
	printf("px4_uart_baudrate:          %d\n", px4_uart_baudrate);
	printf("udp_port_to_px4:            %d\n", udp_port_to_px4);
	printf("udp_port_from_px4:          %d\n", udp_port_from_px4);
	printf("external_fc:                %d\n", external_fc);
	printf("=================================================================");
	printf("\n");
	return 0;
}


int config_file_load(void)
{
	// check if the file exists and make a new one if not
	int ret = json_make_empty_file_with_header_if_missing(CONF_FILE, FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Created new json file: %s\n", CONF_FILE);

	// read the data in
	cJSON* parent = json_read_file(CONF_FILE);
	if(parent==NULL) return -1;

	// parse each item
	json_fetch_int_with_default(	parent, "px4_uart_bus", &px4_uart_bus, DEFAULT_UART_BUS);
	#ifdef APQ8096
	json_fetch_int_with_default(	parent, "px4_uart_backup_bus", &px4_uart_backup_bus, DEFAULT_UART_BACKUP_BUS);
	if(px4_uart_backup_bus==px4_uart_bus){
		fprintf(stderr, "WARNING, backup uart bus can't be the same as the primary\n");
	}
	#endif
	json_fetch_int_with_default(	parent, "px4_uart_baudrate", &px4_uart_baudrate, 921600);
	json_fetch_int_with_default(	parent, "udp_port_to_px4", &udp_port_to_px4, 14556);
	json_fetch_int_with_default(	parent, "udp_port_from_px4", &udp_port_from_px4, 14557);
	json_fetch_bool_with_default(	parent, "external_fc", &external_fc, 0);

	// check if we got any errors in that process
	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse data in %s\n", CONF_FILE);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		// printf("The JSON config file data was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(CONF_FILE, parent, FILE_HEADER);
	}
	cJSON_Delete(parent);
	return 0;
}
