/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h> // for exit()
#include <pthread.h>
#include <sched.h>

#include <c_library_v2/common/mavlink.h> // include before modal_pipe.h
#include <modal_pipe_interfaces.h>
#include <modal_pipe.h>

#include "interface.h"
#include "config_file.h"


#define PROCESS_NAME "voxl-mavlink-server" // to name PID file


// printed if some invalid argument was given
static void _print_usage(void)
{
	printf("\n\
voxl-mavlink-server usually runs as a systemd background service. However, for debug\n\
purposes it can be started from the command line manually with any of the following\n\
debug options. When started from the command line, voxl-mavlink-server will automatically\n\
stop the background service so you don't have to stop it manually\n\
\n\
-c, --load_config_only      Load the config file and then exit right away.\n\
                              This also adds new defaults if necessary. This is used\n\
                              by voxl-configure-voxl-px4 to make sure the config file\n\
                              is up to date without actually starting this service.\n\
-r, --debug_recv            show debug info on uart mavlink packets coming from PX4\n\
-s, --debug_send            show debug info on uart mavlink packets sending to PX4\n\
\n");
	return;
}


static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"load_config_only", no_argument,       0, 'c'},
		{"debug_recv",       no_argument,       0, 'r'},
		{"debug_send",       no_argument,       0, 's'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "crs",
							long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'c':
			config_file_load();
			exit(0);
			break;

		case 'r':
			interface_en_print_debug_recv(1);
			break;

		case 's':
			interface_en_print_debug_send(1);
			break;


		default:
			_print_usage();
			return -1;
		}
	}

	return 0;
}


static void _quit(int ret)
{

	printf("Stopping mavlink module\n");
	interface_stop();
	printf("closing remaining client pipes\n");
	pipe_client_close_all();
	printf("closing remaining server pipes\n");
	pipe_server_close_all();

	// clean up our PID file is all was successful
	printf("Removing PID file\n");
	remove_pid_file(PROCESS_NAME);
	printf("exiting\n");
	exit(ret);
	return;
}




// send mavlink messages received through the mavlink IO control pipe to PX4
static void _mavlink_control_cb(__attribute__((unused)) int ch, char* data,\
								int bytes, __attribute__((unused)) void* context)
{

	// make sure the control pipe handler thread is on the RT scheduler as
	// as time sensitive VIO and Offboard more packets come through here
	static int has_set_priority = 0;
	if(!has_set_priority){
		struct sched_param params;
		params.sched_priority = 80;
		printf("setting thread priority to %d\n", params.sched_priority);
		if(pthread_setschedparam(pthread_self(), SCHED_FIFO, &params)){
			perror("ERROR setting thread priority");
		}
		has_set_priority = 1;
	}

	// validate that the data makes sense
	int n_packets;
	mavlink_message_t* msg_array = pipe_validate_mavlink_message_t(data, bytes, &n_packets);
	if(msg_array == NULL) return;

	// send every packet to px4
	for(int i=0; i<n_packets; i++){
		interface_send_msg(&msg_array[i]);
	}

	return;
}


// initializes everything then waits on signal handler to close
int main(int argc, char* argv[])
{
	if(_parse_opts(argc, argv)) return -1;

	printf("loading our own config file\n");
	if(config_file_load()) return -1;
	config_file_print();

	// make sure another instance isn't running
	// if return value is -3 then a background process is running with
	// higher privaledges and we couldn't kill it, in which case we should
	// not continue or there may be hardware conflicts. If it returned -4
	// then there was an invalid argument that needs to be fixed.
	if(kill_existing_process(PROCESS_NAME, 2.0)<-2) return -1;

	// start signal manager so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal manager\n");
		return -1;
	}

	// make PID file to indicate your project is running
	// due to the check made on the call to rc_kill_existing_process() above
	// we can be fairly confident there is no PID file already and we can
	// make our own safely.
	make_pid_file(PROCESS_NAME);
	if(interface_init()){
		_quit(-1);
	}

	// create the pipe
	pipe_info_t info = { \
		.name        = MAVLINK_PIPE_NAME,\
		.location    = MAVLINK_PIPE_LOCATION,\
		.type        = "mavlink_message_t",\
		.server_name = PROCESS_NAME,\
		.size_bytes  = MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE};

	int flags = SERVER_FLAG_EN_CONTROL_PIPE;
	pipe_server_set_control_cb(MAVLINK_PIPE_CH, _mavlink_control_cb, NULL);
	pipe_server_set_control_pipe_size(MAVLINK_PIPE_CH, \
								MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE,\
								MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);

	if(pipe_server_create(MAVLINK_PIPE_CH, info, flags)){
		fprintf(stderr, "ERROR failed to open mavlink_io pipe\n");
		_quit(-1);
	}

////////////////////////////////////////////////////////////////////////////////
// all threads started, wait for signal manager to stop it
////////////////////////////////////////////////////////////////////////////////
	main_running=1;
	printf("Init complete, entering main loop\n");
	while(main_running){
		usleep(1000000);
		interface_send_heartbeat();
	}

	printf("Starting shutdown sequence\n");


////////////////////////////////////////////////////////////////////////////////
// Stop all the threads
////////////////////////////////////////////////////////////////////////////////

	_quit(0);
	return 0;
}
