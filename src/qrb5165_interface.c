/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>
#include <pthread.h>
#include <unistd.h>	// for  close
#include <errno.h>
#include <poll.h>
#include <modal_pipe_server.h>
#include "interface.h"
#include "config_file.h"

#define UDP_READ_BUF_LEN	(16*1024)
#define MAV_CHAN			0
#define LOCAL_ADDRESS_INT	2130706433



static int running;
static int sockfd;
static int uartfd = -1;
static struct sockaddr_in sockaddr_recv, sockaddr_send, sockaddr_alt;
static struct timeval tv; // for recv timeout
static pthread_t recv_thread_id_local;
static int print_debug_send;
static int print_debug_recv;
static bool use_alt_socket = false;

//                                          1
//                                01234567890
static char external_fc_uart[] = "/dev/ttyHSx";

void interface_en_print_debug_send(int en_print_debug)
{
	printf("Enabling UDP send debugging\n");
	print_debug_send = en_print_debug;
	return;
}

void interface_en_print_debug_recv(int en_print_debug)
{
	printf("Enabling UDP recv debugging\n");
	print_debug_recv = en_print_debug;
	return;
}

int interface_send_msg(mavlink_message_t* msg)
{
	// nothing to do if shutting down
	if(!running) return -1;

	// basic debugging help
	if(print_debug_send){
		printf("SEND  msg ID: %3d sysid:%3d   to port:", msg->msgid, msg->sysid);
		if (external_fc) {
			printf(" %s\n", external_fc_uart);
		} else {
			printf("%6d IP:%s \n",\
					ntohs(sockaddr_send.sin_port), \
					inet_ntoa(sockaddr_send.sin_addr));
		}
	}

	// unpack message into a buffer ready to send
	uint8_t buf[MAVLINK_MAX_PACKET_LEN];
	int bytes = mavlink_msg_to_send_buffer(buf, msg);

	if (external_fc) {
		ssize_t bytes_written = write(uartfd, buf, bytes);
		if (bytes_written < bytes) {
			perror("failed to send to uart socket");
			fprintf(stderr, "Error, UART write incomplete\n");
			return -1;
		}
	} else {
		int ret = sendto(sockfd, buf, bytes, MSG_CONFIRM, \
					(const struct sockaddr*) &sockaddr_send, sizeof(sockaddr_send));
		if(ret!=bytes){
			perror("failed to send to udp socket");
			return -1;
		}
	}

	return 0;
}


int interface_send_heartbeat(void)
{
	mavlink_message_t msg;
	mavlink_msg_heartbeat_pack(0, VOXL_COMPID, &msg, MAV_TYPE_ONBOARD_CONTROLLER, \
								MAV_AUTOPILOT_INVALID, 0, 0, MAV_STATE_ACTIVE);
	if(interface_send_msg(&msg)){
		fprintf(stderr, "ERROR, failed to send heartbeat\n");
		return -1;
	}
	return 0;
}



// thread to read incoming mavlink packets
static void* _recv_local_thread_func(__attribute__((unused)) void *vargp)
{
	printf("starting receive thread\n");

	// general local variables
	int i, bytes_read;
	mavlink_message_t msg;
	mavlink_status_t status;
	int msg_received = 0;
	char buf[UDP_READ_BUF_LEN];
	socklen_t len;

	// keep going until interface_stop sets running to 0
	while(running){

		if (external_fc) {
			struct pollfd fds[1];
			fds[0].fd = uartfd;
			fds[0].events = POLLIN;

			bytes_read = 0;

			// Set timeout at 1 second
			int pollrc = poll(fds, 1, 1000);

			if (pollrc < 0) {
				perror("Error: poll returned negative value");
			} else if (pollrc > 0) {
				if (fds[0].revents & POLLIN) {
					bytes_read = read(uartfd, &buf[0], sizeof(buf));
				} // else printf("Unrecognized poll revent %d\n", fds[0].revents);
			} // else printf("poll timed out\n");
		} else {
			// Receive UDP message from flight controller, this is blocking with timeout
			len = sizeof(sockaddr_recv);
			bytes_read = recvfrom(sockfd, buf, UDP_READ_BUF_LEN, MSG_WAITALL,\
									(struct sockaddr*)&sockaddr_recv, &len);
			// ignore EAGAIN error, that just means the timeout worked
			if (bytes_read < 0 && errno != EAGAIN) {
				perror("ERROR: UDP recvfrom local had a problem");
			}

			if (bytes_read > 0) {
				if (sockaddr_recv.sin_port != htons(udp_port_to_px4)) {
					if ( ! use_alt_socket) printf("Got message from alternate port %u. %d\n", sockaddr_recv.sin_port, bytes_read);
					sockaddr_alt.sin_family = AF_INET; // IPv4
					sockaddr_alt.sin_port = sockaddr_recv.sin_port;
					sockaddr_alt.sin_addr.s_addr = inet_addr("127.0.0.1");
					use_alt_socket = true;
					int ret = sendto(sockfd, buf, bytes_read, MSG_CONFIRM,
									 (const struct sockaddr*) &sockaddr_send,
									 sizeof(sockaddr_send));
					if (ret != bytes_read) {
						perror("failed to forward to px4 udp socket");
					}
					continue;
				} else {
					if (use_alt_socket) {
						int ret = sendto(sockfd, buf, bytes_read, MSG_CONFIRM,
										 (const struct sockaddr*) &sockaddr_alt,
										 sizeof(sockaddr_alt));
						if (ret != bytes_read) {
							perror("failed to send to alt udp socket");
						}
					}
				}
			}
		}

		// do the mavlink byte-by-byte parsing
		msg_received = 0;
		for(i = 0; i < bytes_read; i++) {
			msg_received = mavlink_parse_char(MAV_CHAN, buf[i], &msg, &status);

			// check for dropped packets
			if (status.packet_rx_drop_count != 0) {
				fprintf(stderr,"WARNING: UDP local listener dropped %d packets\n", status.packet_rx_drop_count);
			}

			// msg_received indicates this byte was the end of a complete packet
			if (msg_received){
				if (print_debug_recv) {
					printf("RECV  msg ID: %3d sysid:%3d from port:", msg.msgid, msg.sysid);
					if (external_fc) {
						printf(" %s\n", external_fc_uart);
					} else {
						printf("%6d IP:%s \n",\
								ntohs(sockaddr_recv.sin_port), \
								inet_ntoa(sockaddr_recv.sin_addr));
					}
				}
				// send ALL message out to the pipe
				// even those with CRC/signature errors since the error is likely not an
				// integrity issue, just a mismatch in version, or the user has their own
				// custom mavlink dialect that the receiving end can decode without error.
				pipe_server_write(MAVLINK_PIPE_CH, &msg, sizeof(mavlink_message_t));
			}
		}
	}

	printf("exiting read thread\n");
	return NULL;
}


int interface_init(void)
{
	// UART mode when NOT using voxl-px4 on SDSP
	if (external_fc) {
		// Choose the correct /dev/ttyHSx device
		external_fc_uart[10] = '0' + (px4_uart_bus % 10);

		// Setup the serial port to talk to the external flight controller
		uartfd = open(external_fc_uart, O_RDWR | O_NOCTTY | O_NONBLOCK);
		if (uartfd < 0) {
			perror(__FUNCTION__);
			fprintf(stderr, "ERROR: Open failed on %s\n", external_fc_uart);
			return -1;
		} else {
			printf("Successfully opened %s\n", external_fc_uart);
		}

		struct termios uart_config;

		int termios_state;

		/* fill the struct for the new configuration */
		tcgetattr(uartfd, &uart_config);

		/* properly configure the terminal (see also https://en.wikibooks.org/wiki/Serial_Programming/termios ) */

		//
		// Input flags - Turn off input processing
		//
		// convert break to null byte, no CR to NL translation,
		// no NL to CR translation, don't mark parity errors or breaks
		// no input parity check, don't strip high bit off,
		// no XON/XOFF software flow control
		//
		uart_config.c_iflag &= ~(IGNBRK | BRKINT | ICRNL |
					 INLCR | PARMRK | INPCK | ISTRIP | IXON);
		//
		// Output flags - Turn off output processing
		//
		// no CR to NL translation, no NL to CR-NL translation,
		// no NL to CR translation, no column 0 CR suppression,
		// no Ctrl-D suppression, no fill characters, no case mapping,
		// no local output processing
		//
		// config.c_oflag &= ~(OCRNL | ONLCR | ONLRET |
		//                     ONOCR | ONOEOT| OFILL | OLCUC | OPOST);
		uart_config.c_oflag = 0;

		//
		// No line processing
		//
		// echo off, echo newline off, canonical mode off,
		// extended input processing off, signal chars off
		//
		uart_config.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);

		/* no parity, one stop bit, disable flow control */
		uart_config.c_cflag &= ~(CSTOPB | PARENB | CRTSCTS);

		speed_t uart_speed = B921600;
		switch (px4_uart_baudrate) {
		case 9600:
			uart_speed = B9600;
			break;
		case 19200:
			uart_speed = B19200;
			break;
		case 38400:
			uart_speed = B38400;
			break;
		case 57600:
			uart_speed = B57600;
			break;
		case 115200:
			uart_speed = B115200;
			break;
		case 230400:
			uart_speed = B230400;
			break;
		case 460800:
			uart_speed = B460800;
			break;
		case 921600:
			uart_speed = B921600;
			break;
		default:
			fprintf(stderr, "Error, unsupported baud rate %d.\n", px4_uart_baudrate);
			return -1;
		}

		/* set baud rate */
		if ((termios_state = cfsetispeed(&uart_config, uart_speed)) < 0) {
			perror(__FUNCTION__);
			fprintf(stderr, "ERROR: %d (cfsetispeed)", termios_state);
			return -1;
		}

		if ((termios_state = cfsetospeed(&uart_config, uart_speed)) < 0) {
			perror(__FUNCTION__);
			fprintf(stderr, "ERROR: %d (cfsetospeed)", termios_state);
			return -1;
		}

		if ((termios_state = tcsetattr(uartfd, TCSANOW, &uart_config)) < 0) {
			perror(__FUNCTION__);
			fprintf(stderr, "ERROR: %d (tcsetattr)", termios_state);
			return -1;
		}
	}
	// otherwise UDP mode to connect to voxl-px4 on localhost
	else {
		// set up new socket
		sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if(sockfd < 0) {
			perror("ERROR: local UDP socket creation failed");
			return -1;
		}

		// set timeout for the socket
		tv.tv_sec = 0;
		tv.tv_usec = 500000;
		setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

		// set up receiving port addres
		sockaddr_recv.sin_family = AF_INET; // IPv4
		sockaddr_recv.sin_port = htons(udp_port_from_px4);
		sockaddr_recv.sin_addr.s_addr = inet_addr("127.0.0.1");
		// set up sending port address
		sockaddr_send.sin_family = AF_INET; // IPv4
		sockaddr_send.sin_port = htons(udp_port_to_px4);
		sockaddr_send.sin_addr.s_addr = inet_addr("127.0.0.1");

		// Bind to our receiving port, PX4 binds to it's own receiving port
		if(bind(sockfd, (const struct sockaddr *)&sockaddr_recv, sizeof(sockaddr_recv)) < 0){
			perror("bind failed");
			return -1;
		}
	}

	// flag to the send function and receive thread that the socket is configured
	running = 1;

	// test send
	if(interface_send_heartbeat()){
		close(sockfd);
		running = 0;
		return -1;
	}

	// start the receiving thread
	pthread_create(&recv_thread_id_local, NULL, _recv_local_thread_func, NULL);

	// let the thread start
	usleep(100000);

	return 0;
}


int interface_stop(void)
{
	if(running==0){
		return 0;
	}

	running=0;
	pthread_join(recv_thread_id_local, NULL);

	if (external_fc) {
		close(uartfd);
	} else {
		printf("closing UDP socket\n");
		if(close(sockfd)){
			fprintf(stderr, "ERROR closing socket\n");
			perror("");
		}
	}

	printf("mavlink processing stopped\n");
	return 0;
}
