/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#define _GNU_SOURCE		// for pthread_timedjoin_np and possibly other things
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <voxl_io.h>
#include <modal_pipe_server.h>
#include "interface.h"
#include "config_file.h"


#define BUF_LEN		(32*1024) // big 32k read buffer, each message is 291 bytes
#define BUF_PACKETS	(BUF_LEN/(int)sizeof(mavlink_message_t))



// shared stuff
static pthread_mutex_t uart_write_mutex = PTHREAD_MUTEX_INITIALIZER;
static int print_debug_send = 0;
static int print_debug_recv = 0;
static int did_init = 0;

// primary thread stuff
static pthread_t thread_id;
static int is_primary_running = 0;
static mavlink_message_t* primary_msg_buf;	// big rpc shared memory buffer
static uint8_t* primary_status_buf;			// to hold status of each read message
static int is_primary_bus_open = 0;

// backup bus stuff
static pthread_t backup_thread_id;
static int is_backup_running = 0;
static mavlink_message_t* backup_msg_buf;	// big rpc shared memory buffer
static uint8_t* backup_status_buf;			// to hold status of each read message
static int current_bus = -1;
static int is_backup_bus_open = 0;


void interface_en_print_debug_send(int en_print_debug)
{
	print_debug_send = en_print_debug;
	return;
}

void interface_en_print_debug_recv(int en_print_debug)
{
	print_debug_recv = en_print_debug;
	return;
}



// note, reading must be done in a pthread, not in main()!!!
// the signal handler will interfere with the RPC read call otherwise
static void* _thread_func(void* context)
{
	int empty_read_counter = 0;
	long long int total_sent = 0;

	int bus = (int)context;
	int other_bus;
	mavlink_message_t* msg_buf;
	uint8_t* status_buf;
	int* running;
	int* other_running;
	if(bus==px4_uart_bus){
		// looks like we are the primary reader
		msg_buf = primary_msg_buf;
		status_buf = primary_status_buf;
		running = &is_primary_running;
		other_running = &is_backup_running;
		other_bus = px4_uart_backup_bus;
		printf("Starting primary thread for bus %d\n", bus);
	}
	else if(bus==px4_uart_backup_bus){
		// looks like we are the primary reader
		msg_buf = backup_msg_buf;
		status_buf = backup_status_buf;
		running = &is_backup_running;
		other_running = &is_primary_running;
		other_bus = px4_uart_bus;
		printf("Starting backup thread for bus %d\n", bus);
	}
	else{
		fprintf(stderr, "ERROR invalid bus passed to reader thread, got %d\n", bus);
		return NULL;
	}

	// this is a tight loop around the read function. Read will wait to return
	// untill there is either at least one message available or there was an error
	// this no need to sleep
	while(*running){
		// this blocks for up to 1/2 second waiting for a mavlink message
		int msg_read = voxl_mavparser_read(bus, (char*)msg_buf, status_buf);

		// handle unsuccessful returns
		if(msg_read>=BUF_PACKETS) fprintf(stderr, "WARNING, mavlink buffer full!\n");
		if(msg_read<0){
			fprintf(stderr, "ERROR voxl_mavparser_read returned %d, exiting uart thread\n", msg_read);
			fprintf(stderr, "perhaps something else is using mavparser in the background?\n");
			running = 0;
			return NULL;
		}

		// if we went 1/2 second with no messages we disconnected
		if(msg_read==0){
			empty_read_counter += 1;
			if(empty_read_counter == 3){
				fprintf(stderr, "PX4 DISCONNECTED FROM UART\n");
			}
			continue;
		}
		empty_read_counter = 0;

		// pass each packet with status into message handler
		// note we can't just pass a pointer to the RPC (SDSP) share memory buf
		// (msg_buf) to the UDP and PX4 monitor modules because the shared mem
		// has some strange behaviors and can cause bus errors when doing things
		// other than simple memcopies. So we copy it here onto the stack as we
		// pass the message to the message handler, that's the one and only
		// copy, everything else will read from this copy on the stack.
		int should_send = 1;
		for(int i=0;i<msg_read;i++){
			total_sent++;
			if(msg_buf[i].magic != MAVLINK_STX && msg_buf[i].magic != MAVLINK_STX_MAVLINK1){
				fprintf(stderr, "ERROR message magic number wrong #%lld\n", total_sent);
				should_send = 0;
			}
			if(print_debug_recv){
				printf("PX4->VOXL msgid:%5d sysid:%3d compid:%3d status: %d\n", \
									msg_buf[i].msgid, msg_buf[i].sysid, \
									msg_buf[i].compid, status_buf[i]);
			}
			// else{
			// 	_message_handler(msg_buf[i], status_buf[i]);
			// }
		}
		if(should_send){
			// we got data! tell the other thread to stop
			if(*other_running){
				printf("got first packet on bus %d, stopping the other bus: %d\n", bus, other_bus);
				*other_running = 0;
			}
			current_bus = bus;
			// finally send to px4
			pipe_server_write(MAVLINK_PIPE_CH, msg_buf, msg_read*sizeof(mavlink_message_t));
		}
	}

	printf("thread for bus %d trying to close\n", bus);
	voxl_mavparser_close(bus);
	if(bus==px4_uart_bus) is_primary_bus_open = 0;
	if(bus==px4_uart_backup_bus) is_backup_bus_open = 0;
	printf("thread for bus %d Successfully closed\n", bus);

	return NULL;
}



int interface_send_msg(mavlink_message_t* msg)
{
	static uint8_t* buf = NULL;

	if(!is_primary_running && !is_backup_running){
		return -1;
	}
	if(print_debug_send){
		printf("VOXL->PX4 msgid:%5d sysid:%3d compid:%3d\n", msg->msgid, msg->sysid, msg->compid);
	}

	// protect our bus and write buffer
	pthread_mutex_lock(&uart_write_mutex);

	// make sure out RPC (SDSP) shared memory is allocated
	if(buf==NULL){
		buf = voxl_rpc_shared_mem_alloc(MAVLINK_MAX_PACKET_LEN);
		if(buf==NULL){
			fprintf(stderr, "failed to allocate shared memory buffers for uart send\n");
			pthread_mutex_unlock(&uart_write_mutex);
			return -1;
		}
	}

	// pack the mavlink message
	int msg_len = mavlink_msg_to_send_buffer(buf, msg);
	if(msg_len < 0){
		fprintf(stderr, "ERROR: in interface_send_msg, unable to pack message for sending\n");
		pthread_mutex_unlock(&uart_write_mutex);
		return -1;
	}

	// write out the bus
	if(voxl_uart_write(current_bus, buf, msg_len)!=msg_len){
		fprintf(stderr, "ERROR: in interface_send_msg, failed to write %d bytes to uart %d\n", msg_len, current_bus);
		pthread_mutex_unlock(&uart_write_mutex);
		return -1;
	}

	pthread_mutex_unlock(&uart_write_mutex);
	return 0;
}


int interface_send_heartbeat(void)
{
	mavlink_message_t msg;
	mavlink_msg_heartbeat_pack(0, VOXL_COMPID, &msg, MAV_TYPE_ONBOARD_CONTROLLER, \
								MAV_AUTOPILOT_INVALID, 0, 0, MAV_STATE_ACTIVE);
	if(interface_send_msg(&msg)){
		fprintf(stderr, "ERROR, failed to heartbeat\n");
		return -1;
	}
	return 0;
}




int interface_init(void)
{
	if(did_init){
		fprintf(stderr, "ERROR tried to init apq8096 uart interface, but it's already started\n");
		return -1;
	}

	// initialize the read buffer and mavparser
	primary_msg_buf = (mavlink_message_t*)voxl_rpc_shared_mem_alloc(BUF_LEN);
	primary_status_buf = voxl_rpc_shared_mem_alloc(BUF_PACKETS);
	if(primary_msg_buf==NULL || primary_status_buf==NULL){
		fprintf(stderr, "failed to allocate shared memory buffers for primary mavparser\n");
		return -1;
	}

	// try up to 10 times to start mavparser, this is beacause the sdsp
	// may still be trying to initialize shortly after boot
	int i;
	did_init=0;
	for(i=0;i<10;i++){
		if(voxl_mavparser_init(px4_uart_bus, px4_uart_baudrate, BUF_LEN, 0)){
			fprintf(stderr, "failed to initialize mavparser, trying again\n");
			usleep(500000);
		}
		else{
			printf("Successfully opened primary mavparser on bus %d\n", px4_uart_bus);
			did_init = 1;
			is_primary_running = 1;
			break;
		}
	}

	if(!did_init){
		fprintf(stderr, "ERROR failed to initialize mavparser after 10 attempts on bus %d\n", px4_uart_bus);
		return -1;
	}

	// start current bus as the primary, swap it later if we get data on secondary
	current_bus = px4_uart_bus;
	is_primary_bus_open = 1;

	// start the read thread
	is_primary_running = 1;
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	if(pthread_create(&thread_id, &attr, _thread_func, (void*)px4_uart_bus) != 0){
		fprintf(stderr, "couldn't start thread\n");
		return -1;
	}


	if(px4_uart_backup_bus>=0){
		// initialize the read buffer and mavparser
		backup_msg_buf = (mavlink_message_t*)voxl_rpc_shared_mem_alloc(BUF_LEN);
		backup_status_buf = voxl_rpc_shared_mem_alloc(BUF_PACKETS);
		if(backup_msg_buf==NULL || backup_status_buf==NULL){
			fprintf(stderr, "failed to allocate shared memory buffers for backup mavparser\n");
			return -1;
		}
		if(voxl_mavparser_init(px4_uart_backup_bus, px4_uart_baudrate, BUF_LEN, 0)){
			fprintf(stderr, "WARNING: failed to initialize backup uart bus %d\n", px4_uart_backup_bus);
		}
		else{
			printf("Successfully opened backup mavparser on bus %d\n", px4_uart_backup_bus);
			is_backup_running = 1;
			is_backup_bus_open = 1;
			// start the read thread
			pthread_attr_t attr;
			pthread_attr_init(&attr);
			if(pthread_create(&backup_thread_id, &attr, _thread_func, (void*)px4_uart_backup_bus) != 0){
				fprintf(stderr, "couldn't start thread\n");
				return -1;
			}
		}
	}

	// let the threads start
	usleep(100000);

	return 0;
}


int interface_stop(void)
{
	if(!did_init){
		return 0;
	}

	// do a timed join, 1 second timeout
	printf("waiting for uart reader thread to join\n");
	is_primary_running = 0;
	struct timespec thread_timeout;
	clock_gettime(CLOCK_REALTIME, &thread_timeout);
	thread_timeout.tv_sec += 1;
	errno = pthread_timedjoin_np(thread_id, NULL, &thread_timeout);
	if(errno==ETIMEDOUT){
		fprintf(stderr, "WARNING, timeout joining UART reader thread\n");
	}

	// now close the sdsp mavparser and free its shared memory
	printf("waiting for SDSP mavparser to close\n");
	if(is_primary_bus_open) voxl_mavparser_close(px4_uart_bus);
	voxl_rpc_shared_mem_free((uint8_t*)primary_msg_buf);
	voxl_rpc_shared_mem_free((uint8_t*)primary_status_buf);



	if(px4_uart_backup_bus>=0){
		printf("waiting for uart reader thread to join\n");
		is_backup_running = 0;
		clock_gettime(CLOCK_REALTIME, &thread_timeout);
		thread_timeout.tv_sec += 1;
		errno = pthread_timedjoin_np(backup_thread_id, NULL, &thread_timeout);
		if(errno==ETIMEDOUT){
			fprintf(stderr, "WARNING, timeout joining UART reader thread\n");
		}
		if(is_backup_bus_open) voxl_mavparser_close(px4_uart_backup_bus);
		voxl_rpc_shared_mem_free((uint8_t*)backup_msg_buf);
		voxl_rpc_shared_mem_free((uint8_t*)backup_status_buf);
	}

	return 0;
}
