/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef INTERFACE_H
#define INTERFACE_H

#include <stdint.h>
#include <c_library_v2/common/mavlink.h>

#include <modal_pipe_common.h>

/**
 * All mavlink messages from PX4 (except timesync) are available here
 *
 * Mavlink messages sent to the control pipe are sent to PX4
 */
#define MAVLINK_PIPE_NAME		"mavlink_to_px4"
#define MAVLINK_PIPE_LOCATION	(MODAL_PIPE_DEFAULT_BASE_DIR MAVLINK_PIPE_NAME "/")
#define MAVLINK_PIPE_CH			0

// all messages sent from VOXL are tagged with this component ID to
// differentiate their origin from packets from QGC or PX4
#define VOXL_COMPID		MAV_COMP_ID_VISUAL_INERTIAL_ODOMETRY
#define PX4_COMPID		MAV_COMP_ID_AUTOPILOT1



int interface_init(void);
void interface_en_print_debug_send(int en_print_debug);
void interface_en_print_debug_recv(int en_print_debug);
int interface_send_msg(mavlink_message_t* msg);
int interface_send_heartbeat(void);
int interface_stop(void);


#endif // end #define INTERFACE_H
